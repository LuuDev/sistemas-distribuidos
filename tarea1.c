#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int i, j = -1, aux = -1;
    int num = 3;

    for (i = 0; i < 3; i++)
    {
        if (fork() != 0)
        {
            break;
        }
    }

    if (i == 1)
    {
        aux = fork();
    }

    if (aux == 0)
    {
        for (j = 0; j < 2; j++)
        {
            if (fork() == 0)
                break;
        }
    }

    if(j==0){
        fork();
    }
    while (1)
    {
        /* code */
    }

    return 0;
}
