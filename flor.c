#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
    int tallo_1 = 7;
    int flor = 1;
    int petalos = 5;

    int indice_2, indice_1;

    for (indice_1 = 0; indice_1 < tallo_1; indice_1++)
    {
        if (fork() != 0)
        {
            break;
        }
    }
    if (indice_1 == 5)
    {
        for (indice_2 = 0; indice_2 < flor; indice_2++)
        {
            if (fork() != 0)
            {
                break;
            }
        }
        if (indice_2 == 1)
        {
            for (indice_1 = 0; indice_1 < petalos; indice_1++)
            {
                if (fork() == 0)
                {

                    return 0;
                }
            }
        }
    }
    if (indice_1 == 6)
    {
        for (indice_2 = 0; indice_2 < flor; indice_2++)
        {
            if (fork() != 0)
            {
                break;
            }
        }
        if (indice_2 == 1)
        {
            for (indice_1 = 0; indice_1 < petalos; indice_1++)
            {
                if (fork() == 0)
                {

                    return 0;
                }
            }
        }
    }
    if (indice_1 == 7)
    {
        for (indice_2 = 0; indice_2 < flor; indice_2++)
        {
            if (fork() != 0)
            {
                break;
            }
        }
        if (indice_2 == 1)
        {
            for (indice_1 = 0; indice_1 < petalos; indice_1++)
            {
                if (fork() == 0)
                {

                    return 0;
                }
            }
        }
    }

    while (1)
        ;
}